package VariablesAndDataTypes;

public class MethodOverloading {

    public static void meth(Long n) {
        System.out.println("Long ");
    }

    public static void meth(Short n) {
        System.out.println("Short ");
    }

    public static void meth(int n) {
        System.out.println("int ");
    }

    public static void main(String[] args) {

        short y = 6;   //widening => short < int;
        long z = 7; //can't widen. No need so it will take it as long.

        meth(y);  //call int data type method
        meth(z);  //call short data type method
        
    }





}
